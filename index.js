const express = require('express')
const mongoose = require('mongoose')
require('dotenv').config();

const app = express();
const  port = process.env.PORT || 5000;

//connoct to the database
mongoose.connect(process.env.DB, { useNewUrlParser: true, useUnifiedTopology: true})
.then(() => console.log(`Database connected succesfully`))
.catch(err => console.log(err));

//since mongoose promise is depreciated, we overide it with node's promise 
mongoose.Promise = global.Promise;

app.use((req, res, next) => {
    res.header("access-control-allow-origin", "*");
    res.header("access-control-allow-headers", "origin, x-requested-with, contont-type, accept");
    next();
});

app.use((req, res, next) => {
    res.send('by yogi')
});

app.listen(port, () => {
    console.log(`server running on port ${port}`)
})