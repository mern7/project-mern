const express = require ('express');
const router = express.Router();
const Todo = require('../models/todo');



router.get('/todos',(req, res, next) => {
// this will return all the data, exposing only the id and action field to the client
Todo.find({}, 'action')
.then(data => res.json(data))
.catch(next)
});

router.post('/todos',(req, res, next) => {

});

router.delete('/todos',(req, res, next) => {

});

module.exports = router;