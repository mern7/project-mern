const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create schema for todo
const TodoSchema = new Schema({
    action: {
        type: String,
        require: [true, 'the todo text firld is required']
    }
})

//create model for todo
const todo = mongoose('todo', TodoSchema);

module.exports = Todo;